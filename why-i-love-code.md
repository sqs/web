# Why I love code (and how I started coding)

Code and coding is way more public, more decentralized, and cheaper than any other field:

- Anyone can read and learn from the best code out there for free, with no permission needed.
- Anyone can build things with code and release them to the world for free, with no permission needed.

That's how and why I started coding, and it's why I love code (and the idea of code) so much still.

Here's my personal story of starting to code.

## How I started coding

<small><small><em>This is not some 100%-on-message essay. It's just me writing down what I remember about learning to code.</em></small></small>

I started coding when I was 9 years old (in 1997). I didn't know any other coders at the time.

I was the first coder in my family, but I think some of my grandparents would have gotten into coding if they were born a bit later. My grandmother studied chemistry in college and worked as a copy editor with a keen eye for grammar and syntax. My grandfather was a civil engineer who designed and built many large dams in and around New Mexico.

My parents ran a marketing agency, which meant that we always had computers and (dialup) internet at home. Sometime in 1997 I convinced my parents that the old [Apple Macintosh Performa 6200CD](https://en.wikipedia.org/wiki/Power_Macintosh_6200) should live in my bedroom. All boredom forever vanished from my life because there was just so much to do and learn on the computer---and, thankfully, this was before the internet was so well optimized at wasting human attention.

My earliest memory of getting in deep focus on the computer was an incident when I got in trouble (for what, I don't remember) and the punishment was a 10-minute [time-out](https://en.wikipedia.org/wiki/Time-out_(parenting) in my room. After 10 minutes had passed, my parents told me I could come out. But I told them that I'd like to extend the time-out because I was in the middle of something on the computer and didn't want to stop. My parents didn't give me any more time-outs after that!

The first code I wrote was for The Pirates. I wish that was some cool hacking group, but no, it was my older brother's little league baseball team. My dad (the team's coach) paid me $10 to build a team web page with the game schedules and lineups so that the other players' parents wouldn't need to call our home phone all the time to know where to show up on Saturday mornings. For 9-year-old me, this gig was my first taste of having users (15 players on the team plus their parents), impact (players were on time to practices and games), and money ($10!). Maybe it sounds silly, but it meant a lot to me at the time.

I spent weeks obsessed with learning HTML and building the best web page possible. Everywhere I went, I carried around [HTML for Dummies](https://www.amazon.com/HTML-Dummies-Ed-Tittel/dp/076450214X). One of my favorite programs, [Bryce3D](https://en.wikipedia.org/wiki/Bryce_%28software%29), had a built-in chat room (a feature that was well ahead of its time) where I got advice on making web pages. These friendly people, who were probably not 9 years old and had no idea I was, recommended using [GeoCities](https://en.wikipedia.org/wiki/Yahoo!_GeoCities) (which was a free web host) and sent me URLs to a bunch of free resources on the web (so I no longer needed that HTML for Dummies book).

The web page was ready for the start of baseball season. Of course, I learned then that my client (my dad) thought the $10 paid not just for the initial implementation, but also for near-daily updates to the web page for the entire season. This was a good lesson that all code written must be maintained.

But I loved every minute of it. I especially loved feeling like a part of my older brother's baseball team. Sitting in the bleachers, I wasn't just some spectator. I played some small part in helping the team. I saved some kid the disappointment of having his parent miss the game because they forgot the time and couldn't look it up. I saved some parent the stress of driving to the wrong baseball field because they weren't home the night before to receive a phone call with the update. Again, it sounds small, but for a 9 year old, it was very meaningful.

From that point on, I was hooked on coding!

I found my way to some IRC channels for more help and to chat with other coders. At this point, I still didn't know anybody in real life who knew how to code, so going online was my only option. Random strangers on the internet were so helpful to me as a newbie programmer, and I loved that nobody knew I was 9 years old. People treated me as an adult. (Proof: One day, one of my IRC buddies whose handle was `DJBongHit76` shared a joke about Bill Clinton that completely went over my head at the time but that everyone else in the channel thought was funny. I decided to share it with my parents at the dinner table. It turns out it was wildly inappropriate. My shocked parents asked me where I got that joke from, and I casually told them it was my internet friend `DJBongHit76`. My even-more-shocked parents asked me if I knew what that name meant, and of course I didn't (but it sounded cool!). Mom and dad: thank you for not taking away my internet access after that!)

More and more, I knew enough to be able to help *other* people with web development and coding questions. I spent tons of time in IRC channels helping people. I created a super cringe website called [Fourthdev](https://web.archive.org/web/20000303122630/http://www.fourthdev.com:80/misc/about.htm) and an even cringier "e-zine" called [The Web Design Times](https://web.archive.org/web/20000304175251/http://www.fourthdev.com/wdt/index.htm). Read all those [archives](https://web.archive.org/web/20000304175251/http://www.fourthdev.com/wdt/index.htm) if you're nostalgic about 1990s-era web development. It has gems like:

> [CGI](https://en.wikipedia.org/wiki/Common_Gateway_Interface) enhances your website in a way that nothing else can. You can't make an e-business out of simply HTML and JavaScript. You need CGI, whether you have a personal website or a monopoly (a certain company comes to mind…).

Anyway, maybe I'll post some more details here about how I got more into coding, but the summary is that it was all just from reading the web and chatting with other people on (primarily) IRC. I didn't actually meet any other coder in person until 2005 (when I was 17), when I attended a local event called [Snakes and Rubies](https://www.djangoproject.com/weblog/2005/dec/04/snakes_and_rubies/), where [DHH](https://twitter.com/dhh) presented Ruby on Rails and [Adrian Holovaty](http://www.holovaty.com/) presented Django. [Watch the video](https://www.youtube.com/watch?v=cb9KDt9aXc8&t=9726s) for proof---and it's pretty cool that me, a high-school kid with no credentials and no reputation, could ask public questions of two of the developers I admired most at the time.

## Coding for all

My story is just one data point about how an outsider (anonymous 9--17-year-old me, with no friends or family or teachers who knew how to code) could learn to code and gain independence and meaning from it.

Although I was an "outsider", I grew up with a lot of advantages, such as having supportive parents and a comfortable home life, having a computer and internet access in my bedroom, and doing all this at a time where the internet was more innocent.

Coding isn't equally accessible to everyone. But it's far more accessible than so many other fields that are costly, require permission, and depend on non-public information. That's something that all coders should be proud of!

I want to make coding accessible to more people because [a world where everyone codes](https://about.sourcegraph.com/handbook/company/strategy#purpose) means more technological progress that benefits more people. Two ways I'm trying to help with that are by:

- building [Sourcegraph](https://about.sourcegraph.com) (because code search helps you learn from all the code that's out there, and creates positive incentives for sharing code)
- contributing to [Hack Club](https://hackclub.com/) (because it's the type of coding community I dearly wish existed when I was younger)

(If you have any high-leverage ideas to make coding accessible to more people, and you want advice or funding to implement them, contact me.)
