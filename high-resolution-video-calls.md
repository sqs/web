# High-resolution video calls

Because [Sourcegraph](https://sourcegraph.com) is [all-remote](https://about.sourcegraph.com/company/remote), I do a lot of video calls from my home office. Over time, I've improved the lighting and video quality of my video calling setup. Here's how I did it.

Here's the difference in quality. (It's especially noticeable if you enlarge the photos.)

<div style="display:flex">
<div style="flex:1"><h3 style="margin-top:0">Before</h3>
<img src="media/high-resolution-video-calls-1.jpg" width="100%" />
</div>
<div style="width:5px">&nbsp;</div>
<div style="flex:1"><h3 style="margin-top:0">After</h3>
<img src="media/high-resolution-video-calls-3.jpg" width="100%" />
</div>
</div>

## Equipment list

In all, this will cost around $2,000 USD. If you're looking for something cheaper, just get some of the lighting for around $120-320 USD (lighting makes a huge difference).

- [Sony Alpha A6600 Mirrorless Camera](https://www.amazon.com/gp/product/B07X43B6FD/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B07X43B6FD&linkCode=as2&tag=sqs08-20&linkId=87d1d1643976f594d603f077025e9372)
  - Other cameras will probably work, as long as they have HDMI output.
  - On the Sony cameras, you'll want to disable the info display on the HDMI output (which is configurable in the camera's on-device settings menu) so that your video feed doesn't have all the icon and status overlays.
- [Sony SEL28F20 FE 28mm f/2-22 Lens](https://www.amazon.com/gp/product/B00U29GN6O/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B00U29GN6O&linkCode=as2&tag=sqs08-20&linkId=df10b64da2e296de67f849afb04d7506)
- [Elgato Cam Link 4K](https://www.amazon.com/Elgato-Cam-Link-Broadcast-Camcorder/dp/B07K3FN5MR/ref=as_li_ss_tl?dchild=1&keywords=elgato+cam+link&qid=1588605750&s=electronics&sr=1-3&linkCode=ll1&tag=sqs08-20&linkId=e91a01f7ee06f5af2ebab1703d7100ff&language=en_US), for making the camera's HDMI output appear as a webcam to your computer.
  - If this is out of stock, try:
     - [HonetTek HDMI Video Capture Device](https://www.amazon.com/gp/product/B06XWL7SZD/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B06XWL7SZD&linkCode=as2&tag=sqs08-20&linkId=e83df5320ca968688d53700e32d8d7c9) (I've used this one)
	 - Any other "video capture" device that goes from HDMI to USB, ideally supporting 4K but 1080p is fine.
- [Micro HDMI to HDMI cable](https://www.amazon.com/gp/product/B06WWQ7KLV/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B06WWQ7KLV&linkCode=as2&tag=sqs08-20&linkId=4f0a7828081d0c08573fc06f4a686735), for connecting your camera to the HDMI video capture device.
- [Gonine NP-FZ100 AC Power Adapter Kit](https://www.amazon.com/gp/product/B07D5V8KY5/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B07D5V8KY5&linkCode=as2&tag=sqs08-20&linkId=7c629962ef701202d33c07d835623091), basically a fake battery that plugs into an AC outlet (so you don't need to worry about draining the battery).
- [LED floor lamp](https://www.amazon.com/gp/product/B07WFC14VR/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B07WFC14VR&linkCode=as2&tag=sqs08-20&linkId=cb011bb266c338fa9eaa80bef9f5cd32)
- [LED desk lamp](https://www.amazon.com/gp/product/B07D312DVP/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B07D312DVP&linkCode=as2&tag=sqs08-20&linkId=fda64f24494816f457bc0b8773cf95a2)
- [Elgato Ring Light](https://www.amazon.com/Elgato-Ring-Light-Temperature-app-adjustable/dp/B08GMDQ87T?crid=1TUKHZXXJ62V5&keywords=ring+light+elgato&qid=1645252621&sprefix=ring+light+elgat%2Caps%2C182&sr=8-1&linkCode=ll1&tag=sqs08-20&linkId=5d3a21c2b8b274486672c9b1dcde1ed4&language=en_US&ref_=as_li_ss_tl)
  - This ring light has a camera mount. If you don't want to get the ring light, get the [YOKEPO Computer Shelf](https://www.amazon.com/gp/product/B07JYJ5ZGL/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B07JYJ5ZGL&linkCode=as2&tag=sqs08-20&linkId=de153c907f6cb175976bf3585a84c3b1), which works to rest your camera on top of your computer so the camera angle is natural.
- Audio: I just use AirPods. They work well enough for me.
  - [Matt Mullenweg](https://ma.tt/2020/03/dont-mute-get-a-better-headset/) investigated a bunch of headsets to find the best.
  - Garry Tan [recommends some better audio equipment](https://kit.co/garrytan/badass-telework-setup).

Here's what I look at when I'm on a video call. The camera rests on the [shelf](https://www.amazon.com/gp/product/B07JYJ5ZGL/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B07JYJ5ZGL&linkCode=as2&tag=sqs08-20&linkId=de153c907f6cb175976bf3585a84c3b1) on my iMac, right above the iMac's built-in webcam.

<img src="media/high-resolution-video-calls-camera-light.jpg" width="100%" />

## How I got to my current setup

### Step 1: built-in camera with warm room lighting

I started out using my iMac's built-in FaceTime HD Camera and an [LED floor lamp](https://www.amazon.com/gp/product/B07WFC14VR/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B07WFC14VR&linkCode=as2&tag=sqs08-20&linkId=cb011bb266c338fa9eaa80bef9f5cd32) (in addition to my home office's ceiling lighting) set to the "warm" color temperature.

<img src="media/high-resolution-video-calls-1.jpg" width="100%" />

### Step 2: warm spotlight

Next, I got an [LED desk lamp](https://www.amazon.com/gp/product/B07D312DVP/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B07D312DVP&linkCode=as2&tag=sqs08-20&linkId=fda64f24494816f457bc0b8773cf95a2) (with a warm color temperature) to illuminate me in front of the camera. The result was much better, especially at night.

The spotlight takes a little while to get used to because it's fairly bright. I find that it's actually quite nice to have bright, direct light in the mornings, especially in the winter when the sun isn't out yet.

<img src="media/high-resolution-video-calls-2.jpg" width="100%" />

However, the image is still quite blurry and grainy compared to a photo or video taken on a modern iPhone. I can do better than that! Also, the walls, door, and desk behind me take up most of the frame. I could use a virtual background, but I didn't want to. If I zoomed in more, the image would become even blurrier.

### Step 3: real camera

I lived with the setup above for several months. I looked around for better webcams, but nothing seemed significantly better than my iMac's built-in webcam.

Then I had a Zoom video call with [Kevin Mahaffey](https://kevinmahaffey.com/), who had already figured this all out and had amazing video quality. Credit goes to him for helping me with the rest of this setup. The [equipment list](#equipment-list) has the mirrorless camera and other gear needed for this.

One additional benefit is that your background is optically blurred (because of the lens), which looks great and adds privacy.

<img src="media/high-resolution-video-calls-3.jpg" width="100%" />

## Related reading

- The rest of my [workstation setup](workstation.md)
- [Videoconferencing hardware with low latency video](https://tailscale.com/blog/remote-work/#videoconferencing-hardware) from Avery Pennarun of Tailscale
- [Andreas Klinger's compilation of high-res video setups](https://twitter.com/andreasklinger/status/1238325463300202497)
- [How to Zoom like a CEO](https://www.theinformation.com/articles/how-to-zoom-like-a-ceo)
- [Garry Tan's telework setup](https://kit.co/garrytan/badass-telework-setup), with very similar recommendations
- [Are computers getting faster?](https://danluu.com/input-lag/) specifically on input latency