# Feedback vs. faith

*This is my keynote speech from Berkeley Entrepreneurship Week on Nov. 18, 2013. Thanks to the Startup@Berkeley team for organizing and hosting the event.*

You’re going to hear from a lot of other people this week who have already hit it big in the world of tech startups. Listen to their advice for sure. But I’m going to speak from a different perspective. I haven’t hit it big yet. I’m facing the challenges and decisions you all will face in the early stages of building your own organizations.

So I want to talk about the biggest challenge I’m facing right now, which is dealing with the tension between needing to be receptive to feedback and needing to have faith. That is, how much should you be swayed by feedback from people? And how much should you rely on your own intuition, when people tell you you’re wrong?

This question is not new. Here’s how some well-known investors (Peter Thiel, Paul Graham, and John Vrionis) have phrased it:

> <img width="100%" src="media/feedback-vs-faith-thiel.svg" alt="Venn diagram of 'Ideas that seem stupid' and 'Good ideas', with the intersection labeled 'Good VC investments'" /><br/>
> — Peter Thiel

<span/>

> The most successful founders tend to work on ideas that few beside them realize are good. Which is not that far from a description of insanity, till you reach the point where you see results.
> — [Paul Graham](http://www.paulgraham.com/swan.html)

<span/>

> Great founders are the rare few that make the difficult transition from ignoring everyone (naysayers) to listening intently (customers).
> — [John Vrionis](https://twitter.com/jvrionis/status/349298886520737792)

It’s a really important question because without some faith, you’re not going to create anything new and potentially valuable, but without good feedback you’re not going to get customers and users.

This tension kept coming up in the early stage companies I’ve been at, in 3 important stages: choosing the idea, raising money, and getting customers and users. So I’m going to talk about how this dilemma looks from the founder’s point of view—me, right now, in the first months of my company Sourcegraph, and you in a few months or years—try to convince you that this faith-vs.-feedback tension is the central challenge for founders, and prepare you to use it to your benefit with some mind tricks.

## Doubters

In the early stages of an idea, which is probably where a lot of you are at right now, you go around talking to friends about it, and your default is to get happy when the other person likes the idea and defensive when they don’t. But if you calibrate your idea so that 90% of people like it, it’s probably insufficiently ambitious—and you are overvaluing feedback and neglecting faith. You should be aiming for an idea that a lot of people reject for the same reason, and you should have faith in your ability to overcome that specific issue better than anybody else. 

I first saw this play out when I was the first employee at a company called [Bleacher Report](https://bleacherreport.com/), which is like ESPN.com, but instead of getting career sports journalists to write about the Cal game, they got super passionate amateurs like some of you. Most people said that the quality would always be low, and even if the quality was good, it’d never make money because media companies need predictable content and bylines to sell ads against. Well, Bleacher Report sold for a few hundred million dollars to Time Warner, so clearly people were wrong. The founders had faith that their tech would overcome these issues of quality and ad-friendliness. They were right. By offering a super simple way for anybody to create content instantly and have it be seen by millions of people, they lured successively better sportswriters and could raise their standards for writers. And they developed new kinds of ads that wouldn’t have worked on old media sites.

And yeah, Bleacher Report was controversial in its early days, but that controversy was the initial evidence that it was creating something new and valuable. The important thing is that the areas where people had doubts were precisely where Bleacher Report’s founders believed they could do better than anybody else. And those things, if done well, were very valuable—even the doubters agreed.

But, you know, the reality is that doubters and rejection still hurt. So here’s the secret mind trick I use on myself. You set up a game where you have to get rejected to win. If everyone loves your idea, tell yourself you FAIL. Rejected. The idea’s worthless—it’s unambitious or your friends are too nice. But if most people reject your idea because of the same big problem—one that you have faith you can overcome better than anyone else—then that’s success. That can be a good, defensible business. You have a potential competitive advantage. Train yourself to treat this kind of repeated rejection of the same key premise as success and anything else as failure, and choose your idea so that it gets rejected in this way. You’ll pick a better idea, become more ambitious and get more value out of feedback by doing this.

## Funders

The nice thing about validating your idea in this way, using a mix of feedback and faith, is that it’s what investors are looking for in early stage companies. You’re speaking their language. They want to take a 5% chance on something that could be really big. Show them that your company will get really big if they just assume the faith premise, and then find an investor who thinks you can execute on that premise with 5% probability.

Again, use the mind trick to overcome fear of rejection and your natural tendency to play it safe. You FAIL if every investor thinks your idea is a slam-dunk. That means you have unimaginative investors and no competitive advantage, or you’re raising on bad terms. You succeed only if a limited few investors who know you and the technology best are willing to put faith in your abilities.

Your confidence throughout this process should come from the fact that, if you’ve chosen your idea according to these principles, you have near-100% faith in your ability to execute it better than anyone else, and you just need to convince an investor of a 5% probability of you being the best person to fund.

## Customers/users

This same feedback-vs.-faith tension is the core driver of how we build our product at [Sourcegraph](https://sourcegraph.com). Sourcegraph analyzes and ranks source code in a similar way to how Google organizes and ranks Web pages, and early on most programmers we spoke to doubted it was technologically possible. But from the very beginning, my co-founder Beyang and I, and some experts in the field like Yin Wang (who just joined us), knew it was possible. We knew because we made it work on some of the toughest and most popular codebases.

But it’ll take a lot more work to make Sourcegraph analyze every codebase well. And we can’t know in advance whether some new user will find what they’re looking for, or stumble across something we don’t do well. So for the foreseeable future we’re going to get some bad feedback along with the good. And we’re going to ignore a lot of user requests. But what’s the alternative? That we only allow pre-approved users? Or limit our scope? That’d make it much harder for us to grow and attract the tech talent we need.

Here, again, we use the mind trick to help ourselves make the right decision. All positive feedback, if everyone loves the product, would mean we’re trying to be everything to everyone, or we’re not being sufficiently ambitious. That would be short term success, long term failure. On the other hand, hearing negative feedback about things that we have faith we will address in the future—and positive feedback in areas we’ve already addressed—that means we’re moving more quickly to capture a bigger market. So, we’re focused on increasing the absolute number of people who have good experiences with our product—those are the users and customers who because of our progress, without them even knowing it, have accepted the premise that on day one seemed so far-fetched to the programmers and investors who doubted us.

## Conclusion

So, you’ve seen how this tension between faith and feedback plays a leading role in my life as an entrepreneur in 3 important areas, and how I am trying to overcome it. What it really comes down to is harnessing uncertainty. You must be less uncertain about something valuable than everyone else, and you must get confidence and insight from that incremental certainty.

Now I want to ask you to decide if you agree with my argument that faith-vs.-feedback is the central challenge for entrepreneurs. I hope lots of you disagree, or else my argument wasn’t sufficiently ambitious. Thank you and please get in touch if there’s anything I can do to help.

---

*﻿Thanks to Juan Batiz-Benet, Andy Bromberg, and Beyang Liu.*

*Comments on [Hacker News](https://news.ycombinator.com/item?id=6759644)*