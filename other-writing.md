# Other writing

Here are some things I've written elsewhere:

- [Code search guide](https://codesearchguide.org/) (everything you ever wanted to know about code search)
- [Why Sourcegraph is friends, not competitors, with code hosts](https://about.sourcegraph.com/blog/why-we're-friends-with-code-hosts/)
- [Why code search is still needed in monorepos](https://docs.sourcegraph.com/adopt/code_search_in_monorepos)
- [Sourcegraph raises $50M Series C round led by Sequoia](https://about.sourcegraph.com/blog/series-c-with-sequoia/) (2020)
- [Sourcegraph's Series B: Universal Code Search for every developer](https://about.sourcegraph.com/blog/series-b-universal-code-search/) (2020)
- [Great code search, bad code search](https://about.sourcegraph.com/blog/great-code-search-bad-code-search/)
