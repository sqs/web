# Cooking

## Tips

What I wish I knew when I started cooking (or before I decided to start cooking):

- If someone cooks a delicious meal, ask to pair-cook it with them next time to learn
- Mix the pasta sauce in with the noodles, don't keep them separate
- Fill pasta pots with just enough water to submerge the noodles to save time
- Homemade food takes less time than delivery (if you factor in the uncertainty about delivery times and time to choose, and do some start-of-week prep) and is way better
- Chicken thighs can be marinaded with basically anything, convection baked for 15min, broiled for 2min, and they're delicious
- Zucchini and onion go with basically anything (Benihana-style w/soy sauce, in pasta sauce, in rice, etc.) IMO and my kids luckily agree
- Many things reheat better in a stove/oven/toaster
- 95% of the time restaurants will give you the recipe if you love a dish (esp. fancy restaurants if it's not their signature dish)
- Kids can be helpful starting at ~2yo chopping veggies, mixing, fetching ingredients, etc., and they eat better when they help
- Best recipe sites: [Serious Eats](https://seriouseats.com) and [Caroline Chambers](https://carolinechambers.com)

## Favorite recipes

- [Vietnamese-style baked chicken](https://www.seriouseats.com/vietnamese-style-baked-chicken-recipe)
- [Banana bread](https://www.carolinechambers.com/recipes/breakfast/epic-one-bowl-healthy-ish-banana-bread) (add chocolate chips)
