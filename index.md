<img src="media/sqs.jpg" width="180" height="180" alt="Photo of me" />

I work at [Sourcegraph](https://sourcegraph.com) as CEO. I love programming and want to make it so everyone can code. I studied computer science at Stanford. I'm on the boards of [Sourcegraph](https://sourcegraph.com) and [Hack Club](https://hackclub.com). I live in the San Francisco Bay Area.

Email me at [qslack@gmail.com](mailto:qslack@gmail.com). I'm [@sqs](https://twitter.com/sqs) on Twitter, [@sqs](https://github.com/sqs) on GitHub, and on [LinkedIn](https://www.linkedin.com/in/quinnslack/).

## Pages

- [Context-first approach to code AI](context-first.md)
- [Why I love code (and how I started coding)](why-i-love-code.md)
- [Feedback vs. faith](feedback-vs-faith.md)
- [High-resolution video calls](high-resolution-video-calls.md)
- [Books I read](reading.md)
- [Posts I've written elsewhere](other-writing.md)
- [Workstation setup](workstation.md)
- [Travel gear](travel-gear.md)
- [Self-hosted-first](self-hosted-first.md)
- [Demoing an LLM-based application](llm-demo-tips.md)
- [RFC 8548: Cryptographic Protection of TCP Streams (tcpcrypt)](https://www.rfc-editor.org/rfc/rfc8548.html)
- [Cooking](cooking.md)

---

This site lives in the [gitlab.com/sqs/web](https://gitlab.com/sqs/web) repository and is hosted using [docsite](https://github.com/sourcegraph/docsite).
