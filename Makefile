dev-serve:
	rego -workdir ${HOME}/src/github.com/sourcegraph/docsite github.com/sourcegraph/docsite/cmd/docsite serve -http=:5088

update-docsite:
	docker tag sourcegraph/docsite gcr.io/sqs-web/docsite
	gcloud docker -- push gcr.io/sqs-web/docsite

prod-serve:
	DOCSITE_CONFIG=$$(cat docsite.prod.json) docsite serve -http :5085

deploy:
	gcloud run deploy --project slackorg --region us-central1 --image gcr.io/sqs-web/docsite --platform managed --port 5080 --set-env-vars '^!^'DOCSITE_CONFIG='$(shell cat docsite.prod.json)' --args serve,-http=:5080 docsite
