# Self-hosted-first

> This page is WIP. I'm writing down my thoughts here and other things that I want to be able to link people to.

**Self-hosted-first** startups are those that begin by offering their product as self-hosted (where the customer runs it themselves, instead of using it on "the cloud"). This isn't just about how software is packaged. Self-hosted-first is a more efficient path to product-market fit for many enterprise (especially developer-facing) startups. (It's not the right choice for all companies.)

## Benefits of going self-hosted-first

- **Get good customers sooner:** Your ideal enterprise customer probably doesn't trust your tiny startup with their sensitive data and/or auth tokens. By going self-hosted, your ideal customers can start using your product without needing to trust your tiny startup.
- **Faster to build:** Don't waste time building a multi-tenant application with compartmentalized security, graphical configuration editors, etc. Just ship a simple, single-tenant product to your customers, configured with a text file, and have them deploy it the same way (e.g., with Docker or Kubernetes) that they deploy the open-source internal tools they already use.
- **Focus on product:** Building and running a secure, 24/7-online cloud service is tough and costly, especially if your product involves customer data storage or compute-intensive tasks. Spend your time and money on the product itself, and let your customers manage it and pay their own cloud bills (which are negligible to them, but in aggregate will sink you early on).

---

> ## TODOs
>
> As noted above, this page is WIP. Here are some other notes that I will expand upon sometime in the future.
>
> - Add drawbacks of self-hosted ("when you should NOT go self-hosted")
> - Mention the analogy to mobile apps: "iPhone apps are all effectively self-hosted apps on your phone. You control your data on them (albeit not completely, since you can't easily create a network firewall), they are low-latency and work offline, etc."
> - Talk about the benefits to the user of self-hosted apps, not just the benefits to the creators (although this page is really intended to convince software creators; the argument to users probably merits using a different term than "self-hosted", which itself might actually suggest using a different term altogether that works for all audiences)
> - Respond to ["Self-hosted products are harder to sell and get recurring revenue from than monthly-billed SaaS"](https://twitter.com/alexellisuk/status/1328340634860744707)
> - Talk about how to go from self-hosted to cloud when you've reached enough scale (see [Martin Casado's tweet](https://twitter.com/martin_casado/status/1328768237245394945?s=21))
> - Cite [Tomasz Tunguz's post](https://tomtunguz.com/open-source-cloud-identity-crisis/) about this decision
> - Talk about the point raised in [my tweet](https://twitter.com/sqs/status/1328119903597969409): "Ever wanted to use a product, but decided against it when you get to the screen that asks you to 'Allow SomeApp to read and change your calendar/email/code/etc.'? This hurts (WAY more than people realize) many early startups with otherwise great products."
> - Review other replies to [my self-hosted-first draft tweet](https://twitter.com/sqs/status/1327881142855340032)
> - Explicitly mention our experience with self-hosted-first at [Sourcegraph](https://sourcegraph.com)
>
> ### Links
>
> - [SelfHosted.dev](https://selfhosted.dev/): a similar argument, and a list of self-hosted software (from [Julien Bourdeau](https://twitter.com/julienbourdeau))
> - [awesome-selfhosted](https://github.com/awesome-selfhosted/awesome-selfhosted)